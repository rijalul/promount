<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use View;
use DB;
use Request;

class ItemController extends BlankonController {
    /*
      |--------------------------------------------------------------------------
      | ItemController
      |--------------------------------------------------------------------------
     */

    public function __construct() {
        
        parent::__construct();

        $this->setApp();

        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
            'global/plugins/bower_components/dropzone/downloads/css/dropzone.css',
            'global/plugins/bower_components/jquery.gritter/css/jquery.gritter.css'
        ];

        // page level plugins
        $this->js['plugins'] = [];
    }

    /**
     * Show the application dashboard screen to the user.
     *
     * @return Response
     */
    public function index() {

        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
            'global/plugins/bower_components/datatables/css/dataTables.bootstrap.css',
            'global/plugins/bower_components/datatables/css/datatables.responsive.css',
            'global/plugins/bower_components/fuelux/dist/css/fuelux.min.css'
        ];

        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'admin/css/custom.css',
        ];

        // page level plugins
        $this->js['plugins'] = [
            'global/plugins/bower_components/datatables/js/jquery.dataTables.min.js',
            'global/plugins/bower_components/datatables/js/dataTables.bootstrap.js',
            'global/plugins/bower_components/datatables/js/datatables.responsive.js',
            'global/plugins/bower_components/fuelux/dist/js/fuelux.min.js'
        ];
        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/pages/blankon.table.js',
            'admin/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'DATATABLE | BLANKON - Fullpack Admin Theme');
        
        return view('item/datatable');
    }



    public function element() {

        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
            'global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
            'global/plugins/bower_components/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
            'global/plugins/bower_components/chosen_v1.2.0/chosen.min.css'
        ];
        
        // theme styles
        $this->css['themes'] = [
            'admin/css/reset.css',
            'admin/css/layout.css',
            'admin/css/components.css',
            'admin/css/plugins.css',
            'admin/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'admin/css/custom.css',
        ];

        // page level plugins
        $this->js['plugins'] = [
            'global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js',
            'global/plugins/bower_components/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js',
            'global/plugins/bower_components/holderjs/holder.js',
            'global/plugins/bower_components/bootstrap-maxlength/bootstrap-maxlength.min.js',
            'global/plugins/bower_components/jquery-autosize/jquery.autosize.min.js',
            'global/plugins/bower_components/chosen_v1.2.0/chosen.jquery.min.js'
        ];

        // page level scripts
        $this->js['scripts'] = [
            'admin/js/apps.js',
            'admin/js/pages/blankon.form.element.js',
            'admin/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'FORM ELEMENT | BLANKON - Fullpack Admin Theme');
        
        return view('item/element');
    }


    public function insert(){

// $title=$_POST['title'];
// $note=$_POST['note'];
// $detail=$_POST['detail'];
// $image=$_POST['image'];

$title=Input::get('title');
$price=Input::get('price');
$detail=Input::get('detail');
$description=Input::get('description');
$image=Input::get('image');
$discount=Input::get('discount');
$thumb=Input::get('thumb');
$url=Input::get('url');
$exclusive=Input::get('exclusive');
$featured=Input::get('featured');
$newdeals=Input::get('newDeals');
$topdeals=Input::get('topDeals');
$mostwanted=Input::get('mostWanted');
$toppick=Input::get('topPick');
$sale=Input::get('sale');
$new=Input::get('new');
$location=Input::get('location');
$idmerchant=Input::get('idMerchant');
$idcategory=Input::get('idCategory');


// var dataItems = {'title':title,'detail':detail,'description':description,'price':price,'discount':discount,'image':image,'thumb':thumb,'url':url,'exclusive':exclusive,'featured':featured,'newDeals':newDeals,'topDeals':topDeals,'mostWanted':mostWanted,'topPick':topPick,'sale':sale,'new':New,'location':location,'idMerchant':idMerchant,'idCategory':idCategory};



try{
$values = array('title' => $title,'price' => $price,'detail' => $detail, 'image' => $image,'discount' => $discount,'thumb' => $thumb,'description' => $description, 'url' => $url,'exclusive' => $exclusive,'featured' => $featured, 'newdeals' => $newdeals,'topdeals' => $topdeals,'mostwanted' => $mostwanted, 'toppick' => $toppick,'sale' => $sale,'new' => $new, 'location' => $location,'idmerchant' => $idmerchant,'idcategory' => $idcategory);

DB::table('item')->insert($values);

echo "Insert SUCCESS ";

}catch (Exception $e){
echo "Insert Failed ";

}


}


}

