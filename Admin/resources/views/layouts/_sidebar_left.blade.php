<!--
START @SIDEBAR LEFT
           |=========================================================================================================================|
           |  TABLE OF CONTENTS (Apply to sidebar left class)                                                                        |
           |=========================================================================================================================|
           |  01. sidebar-box               |  Variant style sidebar left with box icon                                              |
           |  02. sidebar-rounded           |  Variant style sidebar left with rounded icon                                          |
           |  03. sidebar-circle            |  Variant style sidebar left with circle icon                                           |
           |=========================================================================================================================|

-->
<aside id="sidebar-left" class="{{ $sidebarClass or 'sidebar-circle' }}">

    <!-- Start left navigation - profile shortcut -->
    <div class="sidebar-content">
        <div class="media">
            <a class="pull-left has-notif avatar" href="{{url('page/profile')}}">
                <img src="http://img.djavaui.com/?create=50x50,4888E1?f=ffffff" alt="admin">
                <i class="online"></i>
            </a>
            <div class="media-body">
                <input type="text" name="idmerchant" id="idmerchant" value="<?php echo  $_SESSION['idMerchant'];?>" style="display: none;">
                <h4 class="media-heading">Hello, <span><?php echo  $_SESSION['name'];?></span></h4>
                <small>Web Designer</small>
            </div>
        </div>
    </div><!-- /.sidebar-content -->
    <!--/ End left navigation -  profile shortcut -->

    <!-- Start left navigation - menu -->


    <ul class="sidebar-menu">
@if(($_SESSION['role']=="admin"))
   <!-- Start navigation - dashboard -->
        <li {!! Request::is('dashboard', '/') ? 'class="active"' : null !!}>
            <a href="{{url('/')}}">
                <span class="icon"><i class="fa fa-home"></i></span>
                <span class="text">Dashboard</span>
                {!! Request::is('dashboard', '/') ? '<span class="selected"></span>' : null !!}
            </a>
        </li>
        <!--/ End navigation - dashboard -->

        <!-- Start category apps -->
        <li class="sidebar-category">

            <span>Data</span>
            <span class="pull-right"><i class="fa fa-database"></i></span>
             <span class="arrow"></span>

        </li>
        <!--/ End category apps -->



 <li {!! Request::is('item','item')? 'class="submenu active"' : 'class="submenu"' !!}>
            <a href="{{url('item')}}">
                <span class="icon"><i class="fa fa-futbol-o"></i></span>
                <span class="text">Item</span>
                <span class="arrow"></span>
                {!! Request::is('item', 'item') ? '<span class="selected"></span>' : null !!}
            </a>
         
        </li>
        <!--/ End navigation - blog -->

        <!-- Start navigation - mail -->
        <li {!! Request::is('category','category')? 'class="submenu active"' : 'class="submenu"' !!}>
            <a href="{{url('category')}}">
                <span class="icon"><i class="fa fa-th-list"></i></span>
                <span class="text">Category</span>
                <span class="arrow"></span>
                {!! Request::is('category', 'category') ? '<span class="selected"></span>' : null !!}
            </a>
        </li>

        <li {!! Request::is('merchant','merchant')? 'class="submenu active"' : 'class="submenu"' !!}>
            <a href="{{url('merchant')}}">
                <span class="icon"><i class="fa fa-briefcase"></i></span>
                <span class="text">Merchant</span>
                <span class="arrow"></span>
                {!! Request::is('merchant', 'merchant') ? '<span class="selected"></span>' : null !!}
            </a>
        </li>
        <!--/ End navigation - blog -->

        <!-- Start navigation - mail -->
        <li {!! Request::is('user','user')? 'class="submenu active"' : 'class="submenu"' !!}>
            <a href="{{url('user')}}">
                <span class="icon"><i class="fa fa-users"></i></span>
                <span class="text">User</span>
                <span class="arrow"></span>
                {!! Request::is('user', 'user') ? '<span class="selected"></span>' : null !!}
            </a>
        </li>
@else
     <!-- Start navigation - dashboard -->
        <li {!! Request::is('dashboard', '/') ? 'class="active"' : null !!}>
            <a href="{{url('/')}}">
                <span class="icon"><i class="fa fa-home"></i></span>
                <span class="text">Dashboard</span>
                {!! Request::is('dashboard', '/') ? '<span class="selected"></span>' : null !!}
            </a>
        </li>
        <!--/ End navigation - dashboard -->

        <!-- Start category apps -->
        <li class="sidebar-category">

            <span>Menu</span>
            <span class="pull-right"><i class="fa fa-database"></i></span>
             <span class="arrow"></span>

        </li>
        <!--/ End category apps -->

    <!-- Start navigation - charts -->

  <li {!! Request::is('viewmyitems','viewmyitems')? 'class="submenu active"' : 'class="submenu"' !!}>
            <a href="{{url('viewmyitems')}}">
                <span class="icon"><i class="fa fa-futbol-o"></i></span>
                <span class="text">Manage Item</span>
                <span class="arrow"></span>
                {!! Request::is('viewmyitems', 'viewmyitems') ? '<span class="selected"></span>' : null !!}
            </a>
         
        </li>

           

 <li {!! Request::is('viewsoldproduct','viewsoldproduct')? 'class="submenu active"' : 'class="submenu"' !!}>
            <a href="{{url('viewsoldproduct')}}">
                <span class="icon"><i class="fa fa-futbol-o"></i></span>
                <span class="text">Sold Item</span>
                <span class="arrow"></span>
                {!! Request::is('viewsoldproduct', 'viewsoldproduct') ? '<span class="selected"></span>' : null !!}
            </a>
         
        </li>
        <!--/ End navigation - blog -->

        <!-- Start navigation - mail -->
        <li {!! Request::is('viewcustomer','viewcustomer')? 'class="submenu active"' : 'class="submenu"' !!}>
            <a href="{{url('viewcustomer')}}">
                <span class="icon"><i class="fa fa-th-list"></i></span>
                <span class="text">Customer</span>
                <span class="arrow"></span>
                {!! Request::is('viewcustomer', 'viewcustomer') ? '<span class="selected"></span>' : null !!}
            </a>
        </li>

        <li {!! Request::is('viewvisiteditem','viewvisiteditem')? 'class="submenu active"' : 'class="submenu"' !!}>
            <a href="{{url('viewvisiteditem')}}">
                <span class="icon"><i class="fa fa-briefcase"></i></span>
                <span class="text">Visited Item</span>
                <span class="arrow"></span>
                {!! Request::is('viewvisiteditem', 'viewvisiteditem') ? '<span class="selected"></span>' : null !!}
            </a>
        </li>
        <!--/ End navigation - blog -->

        <!-- Start navigation - mail -->
     


@endif
    </ul><!-- /.sidebar-menu -->
    <!--/ End left navigation - menu -->

    <!-- Start left navigation - footer -->
    <div class="sidebar-footer hidden-xs hidden-sm hidden-md">
        <a id="setting" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Setting"><i class="fa fa-cog"></i></a>
        <a id="fullscreen" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Fullscreen"><i class="fa fa-desktop"></i></a>
        <a id="lock-screen" data-url="lock-screen" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Lock Screen"><i class="fa fa-lock"></i></a>
        <a id="logout" data-url="signin" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Logout"><i class="fa fa-power-off"></i></a>
    </div><!-- /.sidebar-footer -->
    <!--/ End left navigation - footer -->

</aside><!-- /#sidebar-left -->
<!--/ END SIDEBAR LEFT -->
