@extends('layouts.lay_admin')

<!-- START @PAGE CONTENT -->
@section('content') 
<section id="page-content">
      
    <!-- Start page header -->
    <div class="header-content">
        <h2><i class="fa fa-list-alt"></i> Item Form </h2>

       
        <div class="breadcrumb-wrapper hidden-xs">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{url('/')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                     <a>Item</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li class="active">Update form</li>
            </ol>
        </div><!-- /.breadcrumb-wrapper -->
    </div><!-- /.header-content -->
    <!--/ End page header -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn">

        <div class="row">
            <div class="col-md-12">

                <!-- Start sample validation 1-->
                <div class="panel rounded shadow">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h3 class="panel-title">Update Discount By Id Item</h3>
                        </div>
                        <div class="pull-right">
                            <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                            <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body no-padding">
                        <form class="form-horizontal form-bordered" role="form" id="sample-validation-1">
                            <div class="form-body">

                


                            <!--  -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ID Item <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control input-sm" id="sv1_id" disabled="true">
                                    </div>
                                    
                                </div><!-- /.form-group -->
                               

                            </div><!-- /.form-body --> 

                        </form>

            <!--            
         <div class="col-md-3 pull-right">
                                    <button onclick="select()" class="btn btn-theme">Search</button>
                                    </div> -->


               
                <!--/ End sample validation 1 -->
 

 <form class="form-horizontal form-bordered" role="form" id="sample-validation-1">
                            <div class="form-group">
                                    <label class="col-sm-3 control-label">Title <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" disabled="true" class="form-control input-sm" id="sv1_title">
                                    </div>
                                </div><!-- /.form-group -->
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">Detail <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" disabled="true" class="form-control input-sm" id="sv1_detail">
                                    </div>
                                </div><!-- /.form-group -->

                                 <div class="form-group">
                                    <label class="col-sm-3 control-label">Price <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" disabled="true" class="form-control input-sm" id="sv1_price">
                                    </div>
                                </div><!-- /.form-group -->

                                 <div class="form-group">
                                    <label class="col-sm-3 control-label">Stock <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" disabled="true" class="form-control input-sm" id="sv1_stock">
                                    </div>
                                </div><!-- /.form-group -->

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Discount <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" disabled="true" class="form-control input-sm" id="sv1_discount">
                                    </div>
                                  
                                </div><!-- /.form-group -->




</form>

    <div class="col-md-3 pull-right">
                                    <button onclick="update()" id="updateButton" class="btn btn-success" disabled="true">Update</button>
                                    </div>








            </div>
        </div><!-- /.row -->
     

    </div><!-- /.body-content -->
    </div>
    </div>
    
    <!--/ End body content -->

    <!-- Start footer content -->
    @include('layouts._footer-admin')
    <!--/ End footer content -->

</section><!-- /#page-content -->
@stop
<!--/ END PAGE CONTENT -->





<script>

 
window.onload = function() {

    //alert(window.location.pathname);
    var id=window.location.pathname.split("/");
    // alert(id[2]);
    select(3); 
};
function select(id){
 alert(">>"+id);
 var id=document.getElementById("sv1_id").value;
 var dataItems = {'id':id};
 // console.log(id);
 

  jQuery.ajax({
               url: 'http://localhost:8000/selectmyitemsByID/2',
               type: 'get',
               dataType: 'html',
               success:function(response)
               {             
                var data=JSON.parse(response); 
                document.getElementById("sv1_id").value=data[0].id;
                document.getElementById("sv1_title").value=data[0].title;
                document.getElementById("sv1_detail").value=data[0].detail;
                document.getElementById("sv1_price").value=data[0].price;
                document.getElementById("sv1_stock").value=data[0].stock;
                document.getElementById("sv1_discount").value=data[0].discount;
 
                document.getElementById("sv1_discount").disabled=false;
                document.getElementById("updateButton").disabled=false;

                }

});

// $.ajax({
//         url: "selectmyitems",
//         type: "get",
//         beforeSend: function (xhr) {
//             var token = $('meta[name="csrf_token"]').attr('content');

//             if (token) {
//                   return xhr.setRequestHeader('X-CSRF-TOKEN', token);
//             }
//         },
//         data: dataItems,
//         success: function (response) {
//             console.log(response);

//             document.getElementById("sv1_title").value=response[0].title;
//             document.getElementById("sv1_detail").value=response[0].detail;
//             document.getElementById("sv1_price").value=response[0].price;
//              document.getElementById("sv1_stock").value=response[0].stock;
//             document.getElementById("sv1_discount").value=response[0].discount;

//             document.getElementById("sv1_discount").disabled=false;
//             document.getElementById("updateButton").disabled=false;
    
//             //swal("Success!", ",Data Inserted Succesfully!", "success");
//         },
//         error: function(jqXHR, textStatus, errorThrown) {
//            // console.log(textStatus, errorThrown);
//             swal("Error!", ", Failed Created New Category", "error");
//         }


//     });

}

function update(){
 var discount=document.getElementById("sv1_discount").value;
 var id=document.getElementById("sv1_id").value;
 var dataItems = {'id':id,'discount':discount};

$.ajax({
        url: "updatediscount",
        type: "get",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: dataItems,
        success: function (response) {
      

            document.getElementById("sv1_title").value="";
            document.getElementById("sv1_id").value="";
            document.getElementById("sv1_detail").value="";
            document.getElementById("sv1_price").value="";
             document.getElementById("sv1_stock").value="";
            document.getElementById("sv1_discount").value="";

            document.getElementById("sv1_discount").disabled=true;
            document.getElementById("updateButton").disabled=true;
    
            swal("Success!", response , "success");
        },
        error: function(jqXHR, textStatus, errorThrown) {
           // console.log(textStatus, errorThrown);
            swal("Error!", ", Failed Created New Category", "error");
        }


    });

}



</script>