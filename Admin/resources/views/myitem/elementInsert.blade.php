@extends('layouts.lay_admin')

<!-- START @PAGE CONTENT -->
@section('content')

<section id="page-content">
  
     

    <!-- Start page header -->



    <div class="header-content">
        <h2><i class="fa fa-list-alt"></i> Item Form</h2>
        <div class="breadcrumb-wrapper hidden-xs">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{url('/')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                     <a href="{{url('viewmyitems')}}">Item</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li class="active">Item form</li>
            </ol>
        </div><!-- /.breadcrumb-wrapper -->
    </div><!-- /.header-content -->
    <!--/ End page header -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn">

        <div class="row">
            <div class="col-md-12">

                <!-- Start sample validation 1-->
                <div class="panel rounded shadow">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h3 class="panel-title">New Item</h3>
                        </div>
                        <div class="pull-right">
                            <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                            <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body no-padding">
                        <form class="form-horizontal form-bordered" role="form" id="sample-validation-1">
                            <div class="form-body">

                


                            <!--  -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Title <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control input-sm" id="sv1_title">
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Detail <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control input-sm" id="sv1_detail">
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Description <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control input-sm" id="sv1_description">
                                    </div>
                                </div><!-- /.form-group -->
                                 <div class="form-group">
                                    <label class="col-sm-3 control-label">Price <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control input-sm" id="sv1_price">
                                    </div>
                                </div><!-- /.form-group -->
                                 <div class="form-group">
                                    <label class="col-sm-3 control-label">Stock <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control input-sm" id="sv1_stock">
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Discount <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control input-sm" id="sv1_discount">
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Thumb <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control input-sm" id="sv1_thumb">
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Url <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control input-sm" id="sv1_url">
                                    </div>
                                </div><!-- /.form-group -->


                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Tag <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                     <div class="ckbox ckbox-success">
                                            <input id="checkbox-exclusive" type="checkbox" >
                                            <label for="checkbox-exclusive">Exclusive</label>
                                        </div>
                                        <div class="ckbox ckbox-success">
                                            <input id="checkbox-featured" type="checkbox" >
                                            <label for="checkbox-featured">Featured</label>
                                        </div>
                                        <div class="ckbox ckbox-success">
                                            <input id="checkbox-newdeals" type="checkbox" >
                                            <label for="checkbox-newdeals">New Deals</label>
                                        </div>
                                        <div class="ckbox ckbox-success">
                                            <input id="checkbox-topdeals" type="checkbox" >
                                            <label for="checkbox-topdeals">Top Deals</label>
                                        </div>
                                        <div class="ckbox ckbox-success">
                                            <input id="checkbox-mostwanted" type="checkbox" >
                                            <label for="checkbox-mostwanted">Most Wanted</label>
                                        </div>
                                        <div class="ckbox ckbox-success">
                                            <input id="checkbox-toppick" type="checkbox" >
                                            <label for="checkbox-toppick">Top Pick</label>
                                        </div>
                                        <div class="ckbox ckbox-success">
                                            <input id="checkbox-sale" type="checkbox" >
                                            <label for="checkbox-sale">Sale</label>
                                        </div>
                                        <div class="ckbox ckbox-success">
                                            <input id="checkbox-new" type="checkbox">
                                            <label for="checkbox-new">New</label>
                                        </div>
                                    </div>
                                </div><!-- /.form-group -->
                             <div class="form-group">
                                    <label class="col-sm-3 control-label">Location <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control input-sm" id="sv1_location">
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Id Merchant <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control input-sm" id="sv1_idmerchant">
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group">

                                    <label class="col-sm-3 control-label">Id Category <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control input-sm" id="sv1_idcategory">
                                    </div>
                                </div><!-- /.form-group -->
                            </div><!-- /.form-body -->
                           
                        </form>



                        <div class="form-footer">


                                <div class="col-sm-offset-3">
                                       <?php
         echo Form::open(array('url' => 'uploadfilemerchant','files'=>'true','id'=>'form_upload'));
         echo 'Select the file to upload.';
         echo Form::file('image', array('id' => 'sv1_image'));
         echo Form::close();
      ?>
                                    <button onclick="submitform()" class="btn btn-theme">Submit</button>
                                </div>
                            </div><!-- /.form-footer -->
                    </div>
                </div><!-- /.panel -->
                <!--/ End sample validation 1 -->

            </div>
        </div><!-- /.row -->
     

    </div><!-- /.body-content -->
    <!--/ End body content -->

    <!-- Start footer content -->
    @include('layouts._footer-admin')
    <!--/ End footer content -->

</section><!-- /#page-content -->
@stop
<!--/ END PAGE CONTENT -->





<script>
function submitform(){
var temp=document.getElementById("sv1_image").value;
console.log(temp.length);
if (temp.length!=0){
 insert();
 document.getElementById("form_upload").submit();
 window.location = "viewmyitems"; 
}else{
swal("Error!", ", File must not be empty!", "error");
}

}

function insert(){
 
    var title=document.getElementById("sv1_title").value;
    var detail=document.getElementById("sv1_detail").value;
    var description=document.getElementById("sv1_description").value;
    var price=document.getElementById("sv1_price").value;
    var stock=document.getElementById("sv1_stock").value;
    var discount=document.getElementById("sv1_discount").value;
    var image=document.getElementById("sv1_image").value;
    var thumb=document.getElementById("sv1_thumb").value;
    var url=document.getElementById("sv1_url").value;
    var exclusive="";
    var featured="";
    var newDeals="";
    var topDeals="";
    var mostWanted="";
    var topPick="";
    var sale="";
    var New="";

    if(document.getElementById("checkbox-exclusive").checked){    
        exclusive="1";
    }else{
        exclusive="0";
    }
    if(document.getElementById("checkbox-featured").checked){
        featured="1";
    }else{
        featured="0";
    }
    if(document.getElementById("checkbox-newdeals").checked){
        newDeals="1";
    }else{
        newDeals="0";    
    }
    if(document.getElementById("checkbox-topdeals").checked){
        topDeals="1";
    }else{
        topDeals="0";
    }
    if(document.getElementById("checkbox-mostwanted").checked){
        mostWanted="1";
    }else{
        mostWanted="0";
    }
    if(document.getElementById("checkbox-toppick").checked){
        topPick="1";
    }else{
        topPick="0";
    }
    if(document.getElementById("checkbox-sale").checked){
        sale="1";
    }else{
        sale="0";
    }
    if(document.getElementById("checkbox-new").checked){
        New="1";
    }else{
        New="0";
    }



    var location=document.getElementById("sv1_location").value;
    var idMerchant=document.getElementById("sv1_idmerchant").value;
    var idCategory=document.getElementById("sv1_idcategory").value;
 

    console.log(title);
    console.log(stock);
    console.log(detail);
    console.log(description);
    console.log(price);
    console.log(discount);
    console.log(image);
    console.log(thumb);
    console.log(url);
    console.log(exclusive);
    console.log(featured);
    console.log(newDeals);
    console.log(topDeals);
    console.log(mostWanted);
    console.log(topPick);
    console.log(sale);
    console.log(New);
    console.log(location);
    console.log(idMerchant);
    console.log(idCategory);


// var dataMerchant =JSON.stringify({title:title,detail:detail,image:image,note:note});

var dataItems = {'title':title,'detail':detail,'description':description,'price':price,'stock':stock,'discount':discount,'image':image,'thumb':thumb,'url':url,'exclusive':exclusive,'featured':featured,'newDeals':newDeals,'topDeals':topDeals,'mostWanted':mostWanted,'topPick':topPick,'sale':sale,'new':New,'location':location,'idMerchant':idMerchant,'idCategory':idCategory,'status':0};



$.ajax({
        url: "insertmyitems",
        type: "get",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: dataItems,
        success: function (response) {
           // console.log(response); 
            document.getElementById("sv1_title").value="";
            document.getElementById("sv1_detail").value="";
            document.getElementById("sv1_description").value="";
            document.getElementById("sv1_price").value="";
             document.getElementById("sv1_stock").value="";
            document.getElementById("sv1_discount").value="";
            document.getElementById("sv1_image").value="";
            document.getElementById("sv1_thumb").value="";
            document.getElementById("sv1_url").value="";
            document.getElementById("sv1_location").value="";
            document.getElementById("sv1_idmerchant").value="";
            document.getElementById("sv1_idcategory").value="";
            document.getElementById("checkbox-exclusive").checked=false;
            document.getElementById("checkbox-featured").checked=false;
            document.getElementById("checkbox-newdeals").checked=false;
            document.getElementById("checkbox-topdeals").checked=false;
            document.getElementById("checkbox-mostwanted").checked=false;
            document.getElementById("checkbox-toppick").checked=false;
            document.getElementById("checkbox-sale").checked=false;
            document.getElementById("checkbox-new").checked=false;
    
            swal("Success!", ",Data Inserted Succesfully!", "success");
         
        },
        error: function(jqXHR, textStatus, errorThrown) {
           // console.log(textStatus, errorThrown);
            swal("Error!", ", Check your input data!", "error");
         
        }


    });
// <?php $values = array('id' => 1,'name' => 'Dayle');
// DB::table('users')->insert($values); ?>;

}
</script>