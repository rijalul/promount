@extends('layouts.lay_admin')

<!-- START @PAGE CONTENT -->
@section('content')
<section id="page-content">




         
         


    <!-- Start page header -->
    <div id="tableData" style="visibility:visible">
    <div class="header-content">
        <h2><i class="fa fa-table"></i>Item Datatable </h2>
        <div class="breadcrumb-wrapper hidden-xs">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                     <a href="{{url('/')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                     <a href="{{url('item')}}">Item</a>
                    <i class="fa fa-angle-right"></i>
                </li>
               
            </ol>
        </div><!-- /.breadcrumb-wrapper -->
    </div><!-- /.header-content -->
    <!--/ End page header -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn">

        <div class="row">
            <div class="col-md-12">

                <!-- Start repeater -->
                <div class="panel rounded shadow no-overflow">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h3 class="panel-title">Item List</h3>



                        </div>


                        <div class="pull-right">

                        <!-- FORM INSERT -->



             <a href="{{url('forminsertmyitems')}}">
            <label class="btn btn-sucess"><span class="glyphicon glyphicon-plus"></span></label>   
            </a>

<!-- END FORM INSERT -->

                            <button class="btn btn-sm" data-action="refresh" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Refresh"><i class="fa fa-refresh"></i></button>
                            <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                            <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div><!-- /.panel-heading -->
                    <div class="panel-body">
                        <!-- Start repeater -->

                        <div class="fuelux">
                            <div class="repeater" data-staticheight="400" id="myRepeater">
                                <div class="repeater-header">
                                    <div class="repeater-header-left">
                                        <div class="repeater-search">
                                            <div class="search input-group">
                                                <input type="search" class="form-control" placeholder="Search"/>
                                                          <span class="input-group-btn">
                                                            <button class="btn btn-default" type="button">
                                                                <span class="glyphicon glyphicon-search"></span>
                                                                <span class="sr-only">Search</span>
                                                            </button>
                                                          </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>


    



                                <div class="repeater-viewport">
                                    <div class="repeater-canvas"></div>
                                    <div class="loader repeater-loader"></div>
                                </div>
                                <div class="repeater-footer">
                                    <div class="repeater-footer-left">
                                        <div class="repeater-itemization">
                                            <span><span class="repeater-start"></span> - <span class="repeater-end"></span> of <span class="repeater-count"></span> items</span>
                                            <div class="btn-group selectlist" data-resize="auto">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                    <span class="selected-label">&nbsp;</span>
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li data-value="5"><a href="#">5</a></li>
                                                    <li data-value="10" data-selected="true"><a href="#">10</a></li>
                                                    <li data-value="20"><a href="#">20</a></li>
                                                    <li data-value="50" data-foo="bar" data-fizz="buzz"><a href="#">50</a></li>
                                                    <li data-value="100"><a href="#">100</a></li>
                                                </ul>
                                                <input class="hidden hidden-field" name="itemsPerPage" readonly="readonly" aria-hidden="true" type="text"/>
                                            </div>
                                            <span>Per Page</span>
                                        </div>
                                    </div>
                                    <div class="repeater-footer-right">
                                        <div class="repeater-pagination">
                                            <button type="button" class="btn btn-default btn-sm repeater-prev">
                                                <span class="glyphicon glyphicon-chevron-left"></span>
                                                <span class="sr-only">Previous Page</span>
                                            </button>
                                            <label class="page-label" id="myPageLabel">Page</label>
                                            <div class="repeater-primaryPaging active">
                                                <div class="input-group input-append dropdown combobox">
                                                    <input type="text" class="form-control" aria-labelledby="myPageLabel">
                                                    <div class="input-group-btn">
                                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                            <span class="caret"></span>
                                                            <span class="sr-only">Toggle Dropdown</span>
                                                        </button>
                                                        <ul class="dropdown-menu dropdown-menu-right"></ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="text" class="form-control repeater-secondaryPaging" aria-labelledby="myPageLabel">
                                            <span>of <span class="repeater-pages"></span></span>
                                            <button type="button" class="btn btn-default btn-sm repeater-next">
                                                <span class="glyphicon glyphicon-chevron-right"></span>
                                                <span class="sr-only">Next Page</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ End repeater -->
                    </div><!-- /.panel-body -->
                </div><!-- /.panel -->
                <!--/ End repeater -->



            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->

    </div><!-- /.body-content -->
    <!--/ End body content -->
    </div>

<!-- HIDDEN UPDATE FORM -->
<div id="updateForm" style="visibility:hidden">
 <div class="header-content">
        <h2><i class="fa fa-list-alt"></i> Item Form </h2>

       
        <div class="breadcrumb-wrapper hidden-xs">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{url('/')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                     <a>Item</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li class="active">Update form</li>
            </ol>
        </div><!-- /.breadcrumb-wrapper -->
    </div><!-- /.header-content -->
    <!--/ End page header -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn">

        <div class="row">
            <div class="col-md-12">

                <!-- Start sample validation 1-->
                <div class="panel rounded shadow">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h3 class="panel-title">Update Discount By Id Item</h3>
                        </div>
                        <div class="pull-right">
                            <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                            <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body no-padding">
                        <form class="form-horizontal form-bordered" role="form" id="sample-validation-1">
                            <div class="form-body">

                


                            <!--  -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ID Item <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control input-sm" id="sv1_id" disabled="true">
                                    </div>
                                    
                                </div><!-- /.form-group -->
                               

                            </div><!-- /.form-body --> 

                        </form>

            <!--            
         <div class="col-md-3 pull-right">
                                    <button onclick="select()" class="btn btn-theme">Search</button>
                                    </div> -->


               
                <!--/ End sample validation 1 -->
 

 <form class="form-horizontal form-bordered" role="form" id="sample-validation-1">
                            <div class="form-group">
                                    <label class="col-sm-3 control-label">Title <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text"  class="form-control input-sm" id="sv1_title">
                                    </div>
                                </div><!-- /.form-group -->
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">Detail <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text"  class="form-control input-sm" id="sv1_detail">
                                    </div>
                                </div><!-- /.form-group -->

                                 <div class="form-group">
                                    <label class="col-sm-3 control-label">Price <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control input-sm" id="sv1_price">
                                    </div>
                                </div><!-- /.form-group -->

                                 <div class="form-group">
                                    <label class="col-sm-3 control-label">Stock <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control input-sm" id="sv1_stock">
                                    </div>
                                </div><!-- /.form-group -->

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Discount <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control input-sm" id="sv1_discount">
                                    </div>
                                  
                                </div><!-- /.form-group -->




</form>

    <div class="col-md-3 pull-right">
                                    <button onclick="update()" id="updateButton" class="btn btn-success">Update</button>
                                    </div>
                                    <br>
                                    <br>








            </div>
        </div><!-- /.row -->
     

    </div><!-- /.body-content -->
    </div>
    </div>
    

 </div>


    <!-- Start footer content -->
    @include('layouts._footer-admin')
    <!--/ End footer content -->

</section><!-- /#page-content -->
@stop
<!--/ END PAGE CONTENT -->

<script type="text/javascript">
    function updateData(id){
console.log("ID: "+id);
document.getElementById("tableData").innerHTML=document.getElementById("updateForm").innerHTML;
document.getElementById("sv1_id").value=id;
select(id);
}

function update(){
 var discount=document.getElementById("sv1_discount").value;
 var id=document.getElementById("sv1_id").value;
 var title=document.getElementById("sv1_title").value;
 var detail=document.getElementById("sv1_detail").value;
 var stock=document.getElementById("sv1_stock").value;
 var price=document.getElementById("sv1_price").value;
 var dataItems = {'id':id,'discount':discount,'title':title,'detail':detail,'stock':stock,'price':price};
 
$.ajax({
        url: "updatediscount",
        type: "get",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: dataItems,
        success: function (response) {
            swal("Success!", response , "success");
            location.reload();
        },
        error: function(jqXHR, textStatus, errorThrown) {
           // console.log(textStatus, errorThrown);
            swal("Error!", ", Failed to update data", "error");
            location.reload();
        }


    });

}

function select(id){


 var dataItems = {'id':id};
 // console.log(id);
 

  jQuery.ajax({
               url: 'selectmyitems',
               type: 'get',
               data: dataItems,
               dataType: 'html',
               success:function(response)
               {             
                var data=JSON.parse(response); 
                // document.getElementById("sv1_id").value=data[0].id;
                document.getElementById("sv1_title").value=data[0].title;
                document.getElementById("sv1_detail").value=data[0].detail;
                document.getElementById("sv1_price").value=data[0].price;
                document.getElementById("sv1_stock").value=data[0].stock;
                document.getElementById("sv1_discount").value=data[0].discount;
 
                // document.getElementById("sv1_discount").disabled=false;
                // document.getElementById("updateButton").disabled=false;

                },
                   error: function(jqXHR, textStatus, errorThrown) {
           // console.log(textStatus, errorThrown);
            swal("Error!", ",No Data Found", "error");
          
        }

});

}

function deleteItem(id){
 
var answer=confirm("Are you sure?");
 
if(answer){
 var dataItems = {'id':id};
 console.log(id);
 

  jQuery.ajax({
               url: 'deleteItem',
               type: 'get',
               data: dataItems,
               dataType: 'html',
               success:function(response)
               {             
                //var data=JSON.parse(response); 
                // document.getElementById("sv1_id").value=data[0].id;
                // document.getElementById("sv1_discount").disabled=false;
                // document.getElementById("updateButton").disabled=false;
                 swal("Success", "Data deleted", "success");
                location.reload();
                },
                   error: function(jqXHR, textStatus, errorThrown) {
           // console.log(textStatus, errorThrown);
            swal("Error!", ",No Data Found", "error");
          
        }

});
}


}

</script>