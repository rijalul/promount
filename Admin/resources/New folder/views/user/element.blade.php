@extends('layouts.lay_admin')

<!-- START @PAGE CONTENT -->
@section('content')

<section id="page-content">

    <!-- Start page header -->
    <div class="header-content">
        <h2><i class="fa fa-list-alt"></i>User Form</h2>
        <div class="breadcrumb-wrapper hidden-xs">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{url('/')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                     <a href="{{url('user')}}">User</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li class="active">User Form</li>
            </ol>
        </div><!-- /.breadcrumb-wrapper -->
    </div><!-- /.header-content -->
    <!--/ End page header -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn">

        <div class="row">
            <div class="col-md-12">

                <!-- Start sample validation 1-->
                <div class="panel rounded shadow">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h3 class="panel-title">New User</h3>
                        </div>
                        <div class="pull-right">
                            <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                            <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body no-padding">
                        <form class="form-horizontal form-bordered" role="form" id="sample-validation-1" >
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Username <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control input-sm" id="sv1_username">
                                        <span class="help-block">Just sample username already use : john, peter, bill, jokowi</span>
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Password <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="password" class="form-control input-sm" id="sv1_password">
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Confirm Password <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="password" class="form-control input-sm" id="sv1_password_confirm">
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Email Address <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="email" class="form-control input-sm" id="sv1_email">
                                        <span class="help-block">Just sample email already use : jokowi@jk.co.id, george@bush.gov, bill@gates.com</span>
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Role <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <div class="rdio rdio-theme">
                                            <input id="member" type="radio" name="sv1_role" value="member">
                                            <label for="member">Member</label>
                                        </div>
                                        <div class="rdio rdio-theme">
                                            <input id="merchant" type="radio" name="sv1_role" value="merchant">
                                            <label for="merchant">Merchant</label>
                                        </div>
                                        <div class="rdio rdio-theme">
                                            <input id="admin" type="radio" name="sv1_role" value="admin">
                                            <label for="admin">Admin</label>
                                        </div>
                                        <div class="rdio rdio-theme">
                                            <input id="superadmin" type="radio" name="sv1_role" value="superadmin">
                                            <label for="superadmin">Super Admin</label>
                                        </div>
                                        <label for="sv1_role" class="error"></label>
                                        
                                    </div>
                                </div><!-- /.form-group -->
                            </div><!-- /.form-body -->
                            
                        </form>
                            <div class="form-footer">
                                <div class="col-sm-offset-3">
                                    <button onclick="insert()" class="btn btn-theme">Submit</button>
                                </div>
                            </div><!-- /.form-footer -->
                    </div>
                </div><!-- /.panel -->
                <!--/ End sample validation 1 -->

            </div>
        </div><!-- /.row -->
   

    </div><!-- /.body-content -->
    <!--/ End body content -->

    <!-- Start footer content -->
    @include('layouts._footer-admin')
    <!--/ End footer content -->

</section><!-- /#page-content -->
@stop
<!--/ END PAGE CONTENT -->

<script>
function insert(){
 var name=document.getElementById("sv1_username").value;
    var password=document.getElementById("sv1_password").value;
    var password_confirm=document.getElementById("sv1_password_confirm").value;
    var email=document.getElementById("sv1_email").value;
    var role="";

    if (document.getElementById("member").checked){
        role=document.getElementById("member").value;
    }else if(document.getElementById("merchant").checked){
        role=document.getElementById("merchant").value;
    }else if(document.getElementById("admin").checked){
        role=document.getElementById("admin").value;
    }else if(document.getElementById("superadmin").checked){
        role=document.getElementById("superadmin").value;
    }

console.log(name);
console.log(password);
console.log(password_confirm);
console.log(email);
console.log(role);

if (password==password_confirm){



var dataUser ={'name':name,'email':email,'password':password,'role':role};
$.ajax({
        // url: "insertData/insertMerchant.php",
        url: "userInsert",
        type: "get",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: dataUser,
        success: function (response) {
            console.log(response); 
            document.getElementById("sv1_username").value="";
            document.getElementById("sv1_email").value="";
            document.getElementById("sv1_password").value="";
            document.getElementById("sv1_password_confirm").value="";
            document.getElementById(role).checked=false;
            swal("Congrats!", ", New Category Created!", "success");
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
           swal("Error!", ", Failed Created New Category", "error");
        }


    });






}else{
    alert("Password and Password Confirm did not match! ");
}

}

</script>