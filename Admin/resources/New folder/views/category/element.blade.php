@extends('layouts.lay_admin')

<!-- START @PAGE CONTENT -->
@section('content') 
<section id="page-content">

    <!-- Start page header -->
    <div class="header-content">
        <h2><i class="fa fa-list-alt"></i> Category Form</h2>
        <div class="breadcrumb-wrapper hidden-xs">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="{{url('/')}}">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                     <a href="{{url('category')}}">Category</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li class="active">Category Form </li>
            </ol>
        </div><!-- /.breadcrumb-wrapper -->
    </div><!-- /.header-content -->
    <!--/ End page header -->

    <!-- Start body content -->
    <div class="body-content animated fadeIn">

        <div class="row">
            <div class="col-md-12">

                <!-- Start sample validation 1-->
                <div class="panel rounded shadow">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h3 class="panel-title">New Category</h3>
                        </div>
                        <div class="pull-right">
                            <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                            <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body no-padding">
                        <form class="form-horizontal form-bordered" role="form" id="sample-validation-1">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Title <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control input-sm" id="sv1_title">
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Image</label>
                                    <div class="col-md-8">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <img data-src="holder.js/200x150/blankon/text:Static image" alt="...">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
                                            <div>
                                                <span class="btn btn-primary btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="..." id="sv1_images"></span>
                                                <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /.form-group -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Detail <span class="asterisk">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control input-sm" id="sv1_detail">
                                    </div>
                                </div><!-- /.form-group -->
                                

                            </div><!-- /.form-body -->
                           
                        </form>
                        <div class="form-footer">
                                <div class="col-sm-offset-3">
                                    <button onclick="insert()" class="btn btn-theme">Submit</button>
                                </div>
                            </div><!-- /.form-footer -->
                    </div>
                </div><!-- /.panel -->
                <!--/ End sample validation 1 -->

            </div>
        </div><!-- /.row -->
     

    </div><!-- /.body-content -->
    <!--/ End body content -->

    <!-- Start footer content -->
    @include('layouts._footer-admin')
    <!--/ End footer content -->

</section><!-- /#page-content -->
@stop
<!--/ END PAGE CONTENT -->

<script>

function insert(){
    var title=document.getElementById("sv1_title").value;
    var detail=document.getElementById("sv1_detail").value;
    var image=document.getElementById("sv1_images").value;

console.log(title);
console.log(detail);
console.log(image);


// var dataMerchant =JSON.stringify({title:title,detail:detail,image:image,note:note});

var dataCategory ={'title':title,'detail':detail,'image':image};

$.ajax({
        // url: "insertData/insertMerchant.php",
        url: "categoryInsert",
        type: "get",
        beforeSend: function (xhr) {
            var token = $('meta[name="csrf_token"]').attr('content');

            if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
            }
        },
        data: dataCategory,
        success: function (response) {
            console.log(response); 
            document.getElementById("sv1_title").value="";
            document.getElementById("sv1_detail").value="";
            document.getElementById("sv1_images").value="";
            document.getElementsByClassName("btn btn-danger fileinput-exists")[0].click();
            swal("Congrats!", ", New Category Created!", "success");
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
           swal("Error!", ", Failed Created New Category", "error");

        }


    });
// <?php $values = array('id' => 1,'name' => 'Dayle');
// DB::table('users')->insert($values); ?>;

}
</script>