public static void runCreateJar(String fileInput, String fileTarget) throws IOException
{
  Manifest manifest = new Manifest();
  manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
  JarOutputStream target = new JarOutputStream(new FileOutputStream(fileTarget), manifest);
  add(new File(fileInput), target);
  target.close();
}

private static void add(File source, JarOutputStream target) throws IOException
{
  BufferedInputStream in = null;
  try
  {
    if (source.isDirectory())
    {
      //not optimize for directory
      String name = source.getPath().replace("\\", "/");
      if (!name.isEmpty())
      {
        if (!name.endsWith("/"))
          name += "/";
        /*
        JarEntry entry = new JarEntry(name);
        entry.setTime(source.lastModified());
        target.putNextEntry(entry);
        target.closeEntry();
        */
      }
      for (File nestedFile: source.listFiles())
        add(nestedFile, target);
      return;
    }

    //JarEntry entry = new JarEntry(source.getPath().replace("\\", "/"));
    source.getPath().replace("\\", "/");
    JarEntry entry = new JarEntry(source.getName());
    entry.setTime(source.lastModified());
    target.putNextEntry(entry);
    in = new BufferedInputStream(new FileInputStream(source));

    byte[] buffer = new byte[1024];
    while (true)
    {
      int count = in.read(buffer);
      if (count == -1)
        break;
      target.write(buffer, 0, count);
    }
    target.closeEntry();
  }
  finally
  {
    if (in != null)
      in.close();
  }
}

public static final void writeFile(InputStream in, OutputStream out)
			throws IOException {
		byte[] buffer = new byte[1024];
		int len;

		while ((len = in.read(buffer)) >= 0)
			out.write(buffer, 0, len);

		in.close();
		out.close();
}

public static void zipFiles(File[] files, ZipOutputStream zos){
		try{
			byte[] buffer = new byte[1024];
			//ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
			//out.setLevel(Deflater.DEFAULT_COMPRESSION);
			for (int i = 0; i < files.length; i++){
				FileInputStream in = new FileInputStream(files[i]);
				zos.putNextEntry(new ZipEntry(files[i].getName()));
				int len;
				while ((len = in.read(buffer)) > 0){
					zos.write(buffer, 0, len);
				}
			//out.closeEntry();
			in.close();
			}
			//out.close();
		}
		catch (IllegalArgumentException iae){
			iae.printStackTrace();
		}
		catch (FileNotFoundException fnfe){
			fnfe.printStackTrace();
		}
		catch (IOException ioe){
			ioe.printStackTrace();
		}
}

public static IData wmDoInvoke(String ns, String sn, IData valueData){
	IData output = null;

	try {
			output = Service.doInvoke(ns, sn, valueData);
	} catch (Exception e) {
			throw new RuntimeException(e);
	}		
	return output;
}