<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use View;
use DB;
use Request;


class MerchantController extends BlankonController {
    /*
      |--------------------------------------------------------------------------
      | ItemController
      |--------------------------------------------------------------------------
     */

    public function __construct() {
         session_start();
        parent::__construct();

        $this->setApp();

        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
            'global/plugins/bower_components/dropzone/downloads/css/dropzone.css',
            'global/plugins/bower_components/jquery.gritter/css/jquery.gritter.css'
        ];

        // page level plugins
        $this->js['plugins'] = [];
    }

    /**
     * Show the application dashboard screen to the user.
     *
     * @return Response
     */
    public function index() {

        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
            'global/plugins/bower_components/datatables/css/dataTables.bootstrap.css',
            'global/plugins/bower_components/datatables/css/datatables.responsive.css',
            'global/plugins/bower_components/fuelux/dist/css/fuelux.min.css'
        ];

        // theme styles
        $this->css['themes'] = [
            'global/css/reset.css',
            'global/css/layout.css',
            'global/css/components.css',
            'global/css/plugins.css',
            'global/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'global/css/custom.css',
        ];

        // page level plugins
        $this->js['plugins'] = [
            'global/plugins/bower_components/datatables/js/jquery.dataTables.min.js',
            'global/plugins/bower_components/datatables/js/dataTables.bootstrap.js',
            'global/plugins/bower_components/datatables/js/datatables.responsive.js',
            'global/plugins/bower_components/fuelux/dist/js/fuelux.min.js'
        ];
        // page level scripts
        $this->js['scripts'] = [
            'global/js/apps.js',
            'global/js/pages/blankon.table.merchant.js',
            'global/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'DATATABLE | BLANKON - Fullpack global Theme');
        
        return view('merchant/datatable');
    }
public function getMerchant(){ 
        return json_encode(DB::table('merchant')->get());
}

public function element() {

        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
            'global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
            'global/plugins/bower_components/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
            'global/plugins/bower_components/chosen_v1.2.0/chosen.min.css'
        ];
        
        // theme styles
        $this->css['themes'] = [
            'global/css/reset.css',
            'global/css/layout.css',
            'global/css/components.css',
            'global/css/plugins.css',
            'global/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'global/css/custom.css',
        ];

        // page level plugins
        $this->js['plugins'] = [
            'global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js',
            'global/plugins/bower_components/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js',
            'global/plugins/bower_components/holderjs/holder.js',
            'global/plugins/bower_components/bootstrap-maxlength/bootstrap-maxlength.min.js',
            'global/plugins/bower_components/jquery-autosize/jquery.autosize.min.js',
            'global/plugins/bower_components/chosen_v1.2.0/chosen.jquery.min.js'
        ];

        // page level scripts
        $this->js['scripts'] = [
            'global/js/apps.js',
            'global/js/pages/blankon.form.element.js',
            'global/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'FORM ELEMENT | BLANKON - Fullpack global Theme');
        
        return view('merchant/element');
    }



public function insert(){

// $title=$_POST['title'];
// $note=$_POST['note'];
// $detail=$_POST['detail'];
// $image=$_POST['image'];

$title=Input::get('title');
$note=Input::get('note');
$detail=Input::get('detail');
$image=Input::get('image');



try{
$values = array('title' => $title,'note' => $note,'detail' => $detail, 'image' => $image);
DB::table('merchant')->insert($values);
echo "Insert SUCCESS ";

}catch (Exception $e){
echo "Insert Failed ";

}


}

}
