<?php

namespace App\Http\Controllers;

use View;

class TableController extends BlankonController {
    /*
      |--------------------------------------------------------------------------
      | TableController
      |--------------------------------------------------------------------------
      |
     */

    public function __construct() {

        parent::__construct();

        $this->setApp();

        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
        ];

        // page level plugins
        $this->js['plugins'] = [];
    }

    /**
     * Show the table default screen to the user.
     *
     * @return Response
     */
    public function defaults() {

        // theme styles
        $this->css['themes'] = [
            'global/css/reset.css',
            'global/css/layout.css',
            'global/css/components.css',
            'global/css/plugins.css',
            'global/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'global/css/custom.css',
        ];

        // page level scripts
        $this->js['scripts'] = [
            'global/js/apps.js',
            'global/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'DEFAULT TABLE | BLANKON - Fullpack global Theme');

        return view('table/default');
    }

    /**
     * Show the table color screen to the user.
     *
     * @return Response
     */
    public function color() {

        // theme styles
        $this->css['themes'] = [
            'global/css/reset.css',
            'global/css/layout.css',
            'global/css/components.css',
            'global/css/plugins.css',
            'global/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'global/css/custom.css',
        ];

        // page level scripts
        $this->js['scripts'] = [
            'global/js/apps.js',
            'global/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'COLOR TABLE | BLANKON - Fullpack global Theme');

        return view('table/color');
    }

    /**
     * Show the datatable screen to the user.
     *
     * @return Response
     */
    public function datatable() {

        // page level styles
        $this->css['pages'] = [
            'global/plugins/bower_components/fontawesome/css/font-awesome.min.css',
            'global/plugins/bower_components/animate.css/animate.min.css',
            'global/plugins/bower_components/datatables/css/dataTables.bootstrap.css',
            'global/plugins/bower_components/datatables/css/datatables.responsive.css',
            'global/plugins/bower_components/fuelux/dist/css/fuelux.min.css'
        ];

        // theme styles
        $this->css['themes'] = [
            'global/css/reset.css',
            'global/css/layout.css',
            'global/css/components.css',
            'global/css/plugins.css',
            'global/css/themes/laravel.theme.css' => ['id' => 'theme'],
            'global/css/custom.css',
        ];

        // page level plugins
        $this->js['plugins'] = [
            'global/plugins/bower_components/datatables/js/jquery.dataTables.min.js',
            'global/plugins/bower_components/datatables/js/dataTables.bootstrap.js',
            'global/plugins/bower_components/datatables/js/datatables.responsive.js',
            'global/plugins/bower_components/fuelux/dist/js/fuelux.min.js'
        ];
        // page level scripts
        $this->js['scripts'] = [
            'global/js/apps.js',
            'global/js/pages/blankon.table.js',
            'global/js/demo.js'
        ];

        // pass variable to view
        View::share('css', $this->css);
        View::share('js', $this->js);
        View::share('title', 'DATATABLE | BLANKON - Fullpack global Theme');
        
        return view('table/datatable');
    }

}
