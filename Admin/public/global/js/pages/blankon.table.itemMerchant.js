'use strict';
var BlankonTable = function () {
    
    // =========================================================================
    // SETTINGS APP
    // =========================================================================
    // var globalPluginsPath = BlankonApp.handleBaseURL()+'/assets/global/plugins/bower_components';
    var globalPluginsPath ='/global/plugins/bower_components';

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            BlankonTable.datatable();
        },

        // =========================================================================
        // DATATABLE
        // =========================================================================
        datatable: function () {
            var responsiveHelperAjax = undefined;
            var responsiveHelperDom = undefined;
            var breakpointDefinition = {
                tablet: 1024,
                phone : 480
            };

            var tableAjax = $('#datatable-ajax');
            var tableDom = $('#datatable-dom');

            // Using AJAX
            tableAjax.dataTable({
                autoWidth      : false,
                ajax           : globalPluginsPath+'/datatables/datatable-sample.json',
                preDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelperAjax) {
                        responsiveHelperAjax = new ResponsiveDatatablesHelper(tableAjax, breakpointDefinition);
                    }
                },
                rowCallback    : function (nRow) {
                    responsiveHelperAjax.createExpandIcon(nRow);
                },
                drawCallback   : function (oSettings) {
                    responsiveHelperAjax.respond();
                }
            });

            // Using DOM
            // Remove arrow datatable
            $.extend( true, $.fn.dataTable.defaults, {
                "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0, 1, 2, 5 ] } ]
            } );
            tableDom.dataTable({
                autoWidth        : false,
                preDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelperDom) {
                        responsiveHelperDom = new ResponsiveDatatablesHelper(tableDom, breakpointDefinition);
                    }
                },
                rowCallback    : function (nRow) {
                    responsiveHelperDom.createExpandIcon(nRow);
                },
                drawCallback   : function (oSettings) {
                    responsiveHelperDom.respond();
                }
            });               
            // Repeater
            var columns = [
                {
                    label: 'id',
                    property: 'id',
                    sortable: true
                },
                {
                    label: 'title',
                    property: 'title',
                    sortable: true
                },
                {
                    label: 'detail',
                    property: 'detail',
                    sortable: true
                },
                {
                    label: 'description',
                    property: 'description',
                    sortable: true
                },
                {
                    label: 'normal price',
                    property: 'nprice',
                    sortable: true
                },
                {
                    label: 'discount',
                    property: 'discount',
                    sortable: true
                },
                {
                    label: 'price',
                    property: 'price',
                    sortable: true
                } ,
                {
                    label: 'image',
                    property: 'image',
                    sortable: true
                } ,
                {
                    label: 'url',
                    property: 'url',
                    sortable: true
                },
                {
                    label: 'location',
                    property: 'location',
                    sortable: true
                },  
                {
                    label: 'timestamp',
                    property: 'timestamp',
                    sortable: true
                },
                {
                    label: 'status',
                    property: 'status',
                    sortable: true
                },
                {
                    label: 'action',
                    property: 'action',
                    sortable: true
                }

            ];
            var delays = ['300', '600', '900', '1200'];
           var products = [];
           var productsx = [];
           var id=document.getElementById('idmerchant').value;
           console.log("ID COK: "+id)
            jQuery.ajax({
               url: 'http://localhost:8000/itemGetMyItem/'+id,
               type: 'get',
               dataType: 'html',
               success:function(data)
               {       

                  productsx=JSON.parse(data); 
                  for(var i=0; i<productsx.length; i++){  
                     var p=productsx[i].price-(productsx[i].price*productsx[i].discount/100);
                     var status="";
                     if(productsx[i].status==0){
                        status="pending";
                     }else{
                        status="available";
                     }
                     products.push({"id":productsx[i].id,
                        "title":productsx[i].title,
                        "detail":productsx[i].detail,
                        "description":productsx[i].description,
                        "nprice":productsx[i].price,
                        "discount":productsx[i].discount,
                        "price":p,
                        "image":productsx[i].image,
                        "url":productsx[i].url,
                        "location":productsx[i].location,
                        "timestamp":productsx[i].timestamp, 
                        "status":status, 

                         
                        "action":'<label class="btn btn-failed"><span class="glyphicon glyphicon-pencil" onclick=javascript:updateData('+productsx[i].id+')></span></label> <label class="btn btn-failed"><span class="glyphicon glyphicon-glyphicon glyphicon-trash" onclick=javascript:deleteItem('+productsx[i].id+')></span></label>', 
                  });
                 }
 

            var dataSource, filtering;

            dataSource = function(options, callback){
                var items = filtering(options);
                var resp = {
                    count: items.length,
                    items: [],
                    page: options.pageIndex,
                    pages: Math.ceil(items.length/(options.pageSize || 50))
                };
                var i, items, l;

                i = options.pageIndex * (options.pageSize || 50);
                l = i + (options.pageSize || 50);
                l = (l <= resp.count) ? l : resp.count;
                resp.start = i + 1;
                resp.end = l;

                if(options.view==='list' || options.view==='thumbnail'){
                    if(options.view==='list'){
                        resp.columns = columns;
                        for(i; i<l; i++){  
                            resp.items.push(items[i]);
                        }

                        console.log(resp.items);
                    }else{
                        for(i; i<l; i++){
                            resp.items.push({
                                name: items[i].name,
                                src: items[i].ThumbnailImage
                            });
                        }
                    }

                    setTimeout(function(){
                        callback(resp);
                    }, delays[Math.floor(Math.random() * 4)]);
                }
            };

            filtering = function(options){
                var items = $.extend([], products);
                var search;
                if(options.filter.value!=='all'){
                    items = $.grep(items, function(item){
                        return (item.type.search(options.filter.value)>=0);
                    });
                }
                if(options.search){
                    search = options.search.toLowerCase();
                    items = $.grep(items, function(item){
                        return (
                        (item.codeProduct.toLowerCase().search(options.search)>=0) ||
                        (item.name.toLowerCase().search(options.search)>=0) ||
                        (item.available.toLowerCase().search(options.search)>=0) ||
                        (item.price.toLowerCase().search(options.search)>=0) ||
                        (item.itemCondition.toLowerCase().search(options.search)>=0) ||
                        (item.sold.toLowerCase().search(options.search)>=0) ||
                        (item.review.toLowerCase().search(options.search)>=0) ||
                        (item.type.toLowerCase().search(options.search)>=0)
                        );
                    });
                }
                if(options.sortProperty){
                    items = $.grep(items, function(item){
                        if(options.sortProperty==='id' || options.sortProperty==='height' || options.sortProperty==='weight'){
                            return parseFloat(item[options.sortProperty]);
                        }else{
                            return item[options.sortProperty];
                        }
                    });
                    if(options.sortDirection==='desc'){
                        items.reverse();
                    }
                }

                return items;
            };

            // REPEATER
            $('#repeaterIllustration').repeater({
                dataSource: dataSource
            });

            $('#myRepeater').repeater({
                dataSource: dataSource
            });

            $('#myRepeaterList').repeater({
                dataSource: dataSource
            });

            $('#myRepeaterThumbnail').repeater({
                dataSource: dataSource,
                thumbnail_template: '<div class="thumbnail repeater-thumbnail" style="background: {{color}};"><img height="75" src="{{src}}" width="65"><span>{{name}}</span></div>'
            });

            } 
        }); 
        }

    };

}();

// Call main app init
BlankonTable.init();

