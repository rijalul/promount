## Project Promount ##
### First Step ###
```
#!python
git clone git@bitbucket.org:rijalul/promount.git
```
```
#!python
cd promount
```
```
#!python
npm install
```
```
#!python
composer install
```
```
#!python
cp .env.example .env
```
```
#!python
open .env
```
```
#!python
setting
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```
```
#!python
save .env
```
```
#!python
php artisan key:generate
```
```
#!python
running php artisan serve
```